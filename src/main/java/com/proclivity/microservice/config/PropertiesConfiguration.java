package com.proclivity.microservice.config;

import com.proclivity.common.mappers.CampaignReportDataMapper;
import com.proclivity.common.model.Organization;
import com.proclivity.common.model.budget.CampaignBudget;
import com.proclivity.common.model.campaign.CampaignPerformance;
import com.proclivity.common.model.reports.BillingReportData;
import com.proclivity.common.model.reports.BillingReportReservedData;
import com.proclivity.common.model.reports.CampaignReportData;
import com.proclivity.common.model.reports.CampaignReportDataGrouped;
import com.proclivity.common.model.targeting.Domain;
import com.proclivity.common.model.user.User;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.TypeHandler;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan(basePackages = {"com.proclivity.common.model.*"})
@MapperScan("com.proclivity.common.mappers.*")
public class PropertiesConfiguration {
    private static final String PATH_MAPPERS = "../helix-common/resources/com/proclivity/common/mappers/";


    @Bean
    public DataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/helix_staging");
        dataSource.setUsername("helix");
        dataSource.setPassword("helix-staging");
        return dataSource;
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(getDataSource());

        // add type aliases
        List<Class> typeAliases = new ArrayList<>();
        typeAliases.add(Domain.class);
        typeAliases.add(User.class);
        typeAliases.add(CampaignReportData.class);
        typeAliases.add(CampaignReportDataGrouped.class);
        typeAliases.add(BillingReportData.class);
        typeAliases.add(BillingReportReservedData.class);
        typeAliases.add(CampaignPerformance.class);
        typeAliases.add(CampaignBudget.class);
        typeAliases.add(Organization.class);

        sqlSessionFactory.setTypeAliases(typeAliases.toArray(new Class[]{}));

        // add mapper locations
        List<Resource> mapperLocations = new ArrayList<>();
        mapperLocations.add(new FileSystemResource(PATH_MAPPERS + "CampaignReportDataMapper.xml"));
        sqlSessionFactory.setMapperLocations(mapperLocations.toArray(new Resource[]{}));

        // add type handlers
        List<TypeHandler> typeHandlerList = new ArrayList<>();

        sqlSessionFactory.setTypeHandlers(typeHandlerList.toArray(new TypeHandler[]{}));

        return sqlSessionFactory.getObject();
    }

    @Bean
    public CampaignReportDataMapper crdMapper() throws Exception {
        SqlSessionTemplate sessionTemplate = new SqlSessionTemplate(sqlSessionFactory());
        return sessionTemplate.getMapper(CampaignReportDataMapper.class);
    }

}